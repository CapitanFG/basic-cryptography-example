#include "stdafx.h"
#include "fileUtils.h"
#include <string>
#include <fstream>
#include <sstream>

std::string getTextFromFile(std::string file_name) {
	std::string text;
	std::stringstream str_stream;
	std::ifstream myfile(file_name);

	str_stream << myfile.rdbuf();
	text = str_stream.str();

	return text;
}