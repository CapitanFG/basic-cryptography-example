#include "stdafx.h"
#include "Caesar.h"
#include "utils.h"
#include <iostream>

Caesar::Caesar() {

	letter_freq['a'] = 0.080;
	letter_freq['b'] = 0.015;
	letter_freq['c'] = 0.030;
	letter_freq['d'] = 0.040;
	letter_freq['e'] = 0.130;
	letter_freq['f'] = 0.020;
	letter_freq['g'] = 0.015;
	letter_freq['h'] = 0.060;
	letter_freq['i'] = 0.065;
	letter_freq['j'] = 0.005;
	letter_freq['k'] = 0.005;
	letter_freq['l'] = 0.035;
	letter_freq['m'] = 0.030;
	letter_freq['n'] = 0.070;
	letter_freq['o'] = 0.080;
	letter_freq['p'] = 0.020;
	letter_freq['q'] = 0.002;
	letter_freq['r'] = 0.065;
	letter_freq['s'] = 0.060;
	letter_freq['t'] = 0.090;
	letter_freq['u'] = 0.030;
	letter_freq['v'] = 0.010;
	letter_freq['w'] = 0.015;
	letter_freq['x'] = 0.005;
	letter_freq['y'] = 0.020;
	letter_freq['z'] = 0.002;
}

// just for lower case letters
std::string Caesar::cryptDecrypt(std::string text, int gap) {
	std::string resp("");
	for (unsigned int i = 0; i < text.length(); ++i) {
		char c = text.at(i);
		if (c < 97 || c > 122) {
			resp += c;
		} 
		else {
			int ascii_code = c + gap;
			if (ascii_code < 97) ascii_code = 123 - (97 - ascii_code);
			if (ascii_code > 122) ascii_code = ascii_code - 122 + 96;
			resp += (char)ascii_code;
		}
	}
	return resp;
}

// estimate gap
int Caesar::get_gap(std::string cryptText) {
	std::map<char, double> text_freq = letter_freq_counter(cryptText);
	modify_freq_map(text_freq);
	return compute_gap(letter_freq ,text_freq);
}

Caesar::~Caesar()
{
}
