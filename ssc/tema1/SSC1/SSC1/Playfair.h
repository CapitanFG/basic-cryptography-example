#pragma once
#include <string>

class Playfair
{
private:
	std::string key_word;
	char key_matrix[5][5];
public:
	Playfair();
	Playfair(std::string key_word);
	std::string crypt(std::string text);
	std::string decrypt(std::string text);
	~Playfair();
};

