#include "stdafx.h"
#include "utils.h"
#include <iostream>

std::map<char, double> letter_freq_counter(std::string& text) {
	std::map<char, double> text_freq;

	for (int i = 0; i < text.length(); ++i) {
		char c = text.at(i);

		// filter non letter chars
		if (c < 97 || c > 122)
			continue;
		text_freq[c]++;
	}
	return text_freq;
}

int number_of_letters(std::map<char, double>& the_map) {
	int res = 0;
	for (int i = 'a'; i <= 'z'; ++i) {
		res += the_map[i];
	}
	return res;
}

void modify_freq_map(std::map<char, double>& the_map) {
	int size = number_of_letters(the_map);
	for (int i = 'a'; i <= 'z'; ++i) {
		the_map[i] = the_map[i] / size;
	}
}

std::pair<char, double> max_freq_map(std::map<char, double>& the_map, int min_val) {
	double currentMax = 0;
	char arg_max = 0;
	std::pair<char, double> resp;
	for (auto it = the_map.cbegin(); it != the_map.cend(); ++it) {
		if (it->second < min_val)
			continue;
		if (it->second > currentMax) {
			arg_max = it->first;
			currentMax = it->second;
			resp = std::make_pair(it->first, it->second);
		}
	}
	return resp;
}
char max_freq_map_2(std::map<int, int>& the_map) {
	unsigned currentMax = 0;
	unsigned arg_max = 0;
	for (auto it = the_map.cbegin(); it != the_map.cend(); ++it) {
		if (it->second > currentMax) {
			arg_max = it->first;
			currentMax = it->second;
		}
	}
	return arg_max;
}

int min_freq_map(std::map<int, double>& the_map) {
	double currentMax = 100000;
	int arg_max = 0;
	for (auto it = the_map.cbegin(); it != the_map.cend(); ++it) {
		if (it->second < currentMax) {
			arg_max = it->first;
			currentMax = it->second;
		}
	}
	return arg_max;
}

std::list<char> all_posible_gaps(std::map<char, double>& the_map, std::pair<char, double>& first_pair) {
	std::list<char> posible_gap;
	posible_gap.push_back(first_pair.first);
	double freq = first_pair.second;
	for (auto it = the_map.cbegin(); it != the_map.cend(); ++it) {
		if (it->second == freq)
			posible_gap.push_back(it->first);
	}
	return posible_gap;
}

std::map<char, double> transform_map(std::map<char, double> computed_freq) {
	std::map<char, double> res;
	for (int i = 'a'; i <= 'z'; ++i) {
		res[i] = computed_freq[(i + 1 > 'z') ? 'a' : i + 1];
	}
	return res;
}

int get_difference(std::map<char, double> letter_freq, std::map<char, double> computed_freq) {
	int difference = 0;
	for (int i = 'a'; i <= 'z'; ++i) {
		difference += abs(letter_freq[i] - computed_freq[i]);
	}
	return difference;
}

int compute_gap(std::map<char, double> letter_freq, std::map<char, double> computed_freq) {
	std::map<int, double> diff_list;
	std::map<char, double> temp;
	for (int i = 0; i < letter_freq.size(); ++i) {
		diff_list[i] = get_difference(letter_freq, temp);
		temp = transform_map(temp);
	}
	return min_freq_map(diff_list);
}

void printMap(std::map<int, int> the_map) {
	for (auto it = the_map.cbegin(); it != the_map.cend(); ++it) {
		std::cout << "key: " << it->first << "; value: " << it->second << std::endl;
	}
}

std::pair<int, int> letter_indexes(char key_matrix[5][5], char letter) {
	if (letter == 'j')
		letter = 'i';
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {
			if (key_matrix[i][j] == letter)
				return std::make_pair(i, j);
		}
	}
}