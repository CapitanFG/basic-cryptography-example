// SSC1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Caesar.h"
#include "Polybius.h"
#include "Playfair.h"
#include "fileUtils.h"

#include <iostream>

int main()
{
	std::string filename = "test.txt";
	std::string text = getTextFromFile(filename);
	int gap = 3;

	Caesar* test1 = new Caesar();
	Playfair* test2 = new Playfair("zebra");
	Polybius* test3 = new Polybius("zebra");

	std::string cryptedText1 = test1->cryptDecrypt(text, gap);
	std::string cryptedText2 = test2->crypt(cryptedText1);
	std::string cryptedText3 = test3->crypt(cryptedText2);

	std::string decryptedText3 = test3->decrypt(cryptedText3);
	std::string decryptedText2 = test2->decrypt(decryptedText3);
	std::string decryptedText1 = test1->cryptDecrypt(decryptedText2, (-1) * gap);

	std::cout << decryptedText1 << std::endl;

    return 0;
}

