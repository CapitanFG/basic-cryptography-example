#include "stdafx.h"
#include "Polybius.h"
#include "utils.h"


Polybius::Polybius()
{
}

Polybius::Polybius(std::string key_word) {
	this->key_word = key_word;

	int key_word_length = key_word.length();
	char letter = 'a';

	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {
			if (i * 5 + j < key_word_length) {
				key_matrix[i][j] = key_word.at(i * 5 + j);
			}
			else {
				while (key_word.find(letter) != std::string::npos 
					|| letter == 'j') {
					letter++;
				}
				key_matrix[i][j] = letter++;
			}
		}
	}
}

std::string Polybius::crypt(std::string text) {
	std::string resp("");
	for (unsigned int i = 0; i < text.length(); ++i) {
		char c = text.at(i);
		if (c < 97 || c > 122) {
			resp += c;
		}
		else {
			std::pair<int, int> indexes = letter_indexes(key_matrix, c);
			resp += (char)(indexes.first + 65);
			resp += (char)(indexes.second + 65);
		}
	}
	return resp;
}

std::string Polybius::decrypt(std::string text) {
	std::string resp("");
	for (unsigned int i = 0; i < text.length(); i++) {
		char c_i = text.at(i);
		if (c_i < 'A' || c_i > 'E') {
			resp += c_i;
		}
		else {
			char c_j = text.at(++i);
			resp += key_matrix[c_i - 65][c_j - 65];
		}
	}
	return resp;
}

Polybius::~Polybius()
{
}
