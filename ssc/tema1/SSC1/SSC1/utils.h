#pragma once
#include <map>
#include <string>
#include <list>

std::map<char, double> letter_freq_counter(std::string& text);
void modify_freq_map(std::map<char, double>& the_map);
char max_freq_map_2(std::map<int, int>& the_map);
void printMap(std::map<int, int> the_map);
int compute_gap(std::map<char, double> letter_freq, std::map<char, double> computed_freq);
std::pair<int, int> letter_indexes(char key_matrix[5][5], char letter);