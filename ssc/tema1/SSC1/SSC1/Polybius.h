#pragma once
#include <string>

class Polybius
{
private:
	std::string key_word;
	char key_matrix[5][5];
public:
	Polybius();
	Polybius(std::string key_word);
	std::string crypt(std::string text);
	std::string decrypt(std::string text);
	~Polybius();
};

