#pragma once

#include <string>
#include <map>

class Caesar
{
private:
	std::string text;

	std::map<char, double> letter_freq;
public:
	Caesar();

	std::string cryptDecrypt(std::string text, int gap);
	int get_gap(std::string);

	~Caesar();
};

