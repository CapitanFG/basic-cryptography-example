#include "stdafx.h"
#include "Playfair.h"
#include "utils.h"


Playfair::Playfair()
{
}

Playfair::Playfair(std::string key_word) {
	this->key_word = key_word;

	int key_word_length = key_word.length();
	char letter = 'a';

	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {
			if (i * 5 + j < key_word_length) {
				key_matrix[i][j] = key_word.at(i * 5 + j);
			}
			else {
				while (key_word.find(letter) != std::string::npos
					|| letter == 'j') {
					letter++;
				}
				key_matrix[i][j] = letter++;
			}
		}
	}
}

std::string Playfair::crypt(std::string text) {
	std::string resp("");
	for (unsigned int i = 0; i < text.length(); ++i) {
		std::string insertFirst = "";
		std::string insertBetween = "";
		char c_first;
		do {
			if (i >= text.length())
				break;
			c_first = text.at(i++);
			if (c_first < 97 || c_first > 122)
				insertFirst += c_first;
		} while (c_first < 97 || c_first > 122);
		char c_second;
		do {
			if (i >= text.length()) {
				c_second = 'x';
			}
			else {
				c_second = text.at(i++);
			}

			if (c_second < 97 || c_second > 122)
				insertBetween += c_second;

		} while (c_second < 97 || c_second > 122);
		--i;

		// first
		if (c_first == c_second) {
			c_second = 'x';
			i--;
		}

		if (c_first == 'j')
			c_first = 'i';
		if (c_second == 'j')
			c_second = 'i';

		std::pair<int, int> indexes_first = letter_indexes(key_matrix, c_first);
		std::pair<int, int> indexes_second = letter_indexes(key_matrix, c_second);

		resp += insertFirst;

		// second
		if (indexes_first.first == indexes_second.first) {
			int col_1 = indexes_first.second + 1;
			if (col_1 > 4)
				col_1 = 0;
			resp += key_matrix[indexes_first.first][col_1];

			resp += insertBetween;

			int col_2 = indexes_second.second + 1;
			if (col_2 > 4)
				col_2 = 0;
			resp += key_matrix[indexes_second.first][col_2];
		}
		// third
		else if (indexes_first.second == indexes_second.second) {
			int col_1 = indexes_first.first + 1;
			if (col_1 > 4)
				col_1 = 0;
			resp += key_matrix[col_1][indexes_first.second];

			resp += insertBetween;

			int col_2 = indexes_second.first + 1;
			if (col_2 > 4)
				col_2 = 0;
			resp += key_matrix[col_2][indexes_second.second];
		} // forth
		else {
			resp += key_matrix[indexes_first.first][4 - indexes_first.second];

			resp += insertBetween;

			resp += key_matrix[indexes_second.first][4 - indexes_second.second];
		}
	}
	return resp;
}

std::string Playfair::decrypt(std::string text) {
	std::string resp("");
	for (unsigned int i = 0; i < text.length(); i++) {
		std::string insertFirst = "";
		std::string insertBetween = "";
		char c_first;
		do {
			if (i >= text.length())
				break;
			c_first = text.at(i++);
			if (c_first < 97 || c_first > 122)
				insertFirst += c_first;
		} while (c_first < 97 || c_first > 122);
		char c_second;
		do {
			if (i >= text.length()) {
				c_second = 'x';
			}
			else {
				c_second = text.at(i++);
			}

			if (c_second < 97 || c_second > 122)
				insertBetween += c_second;

		} while (c_second < 97 || c_second > 122);
		--i;

		if (c_first == 'j')
			c_first = 'i';
		if (c_second == 'j')
			c_second = 'i';

		std::pair<int, int> indexes_first = letter_indexes(key_matrix, c_first);
		std::pair<int, int> indexes_second = letter_indexes(key_matrix, c_second);

		resp += insertFirst;

		// second
		if (indexes_first.first == indexes_second.first) {
			int col_1 = indexes_first.first - 1;
			if (col_1 < 0)
				col_1 = 4;
			resp += key_matrix[indexes_first.first][col_1];

			resp += insertBetween;

			int col_2 = indexes_second.first - 1;
			if (col_2 < 0)
				col_2 = 4;
			resp += key_matrix[indexes_second.first][col_2];
		}
		// third
		else if (indexes_first.second == indexes_second.second) {
			int col_1 = indexes_first.first - 1;
			if (col_1 < 0)
				col_1 = 4;
			resp += key_matrix[col_1][indexes_first.second];

			resp += insertBetween;

			int col_2 = indexes_second.first - 1;
			if (col_2 < 0)
				col_2 = 4;
			resp += key_matrix[col_2][indexes_second.second];
		} // forth
		else {
			resp += key_matrix[indexes_first.first][4 - indexes_first.second];

			resp += insertBetween;

			resp += key_matrix[indexes_second.first][4 - indexes_second.second];
		}
	}
	return resp;
}


Playfair::~Playfair()
{
}
